<?php
	$dbhost = 'localhost';
	$dbuser = 'root';
	$dbpass = '';
	$dbname = 'prueba';
	$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname);

	if($mysqli->connect_errno)
        die('No se puede conectar: ' . $mysqli->connect_error);

    $mysqli->set_charset("utf8");
    $sql = 'SELECT * FROM persona';
	$resultado = $mysqli->query($sql);

	if(!$resultado)
		die('No se pudo realizar la consulta: ' . $mysqli->error);

	$datos = array();

	while($row = $resultado->fetch_assoc()) {
		$datos[] = array( 
			'nombre' => $row['nombre'],
			'edad' => $row['edad']
		);
	}

    $resultado->free();
	$mysqli->close();

	header('Content-Type: application/json');
	$myJSON = json_encode($datos, JSON_UNESCAPED_UNICODE);
	echo $myJSON;
?>