<?php
	function getInputIntentos($intentos) {
		return "<input type='hidden' name='intentos' value='{$intentos}'/>";
	}

	$num_intentos = 3;
	$adivino = false;
	$quedan_intentos = true;
	$buscado = 7;
	$msg_intento = '';

	if(!empty($_GET)) { // Si se envío el formulario
		$num_intentos = $_GET['intentos'];

		if(isset($_GET['numero_prueba']) && $_GET['numero_prueba'] != '') { // Si se trató de adivinar un número
			$num_intentos--;

			if($num_intentos == 0)
				$quedan_intentos = false;

			if($_GET['numero_prueba'] == $buscado)
				$adivino = true;
			else if($_GET['numero_prueba'] > $buscado)
				$msg_intento = "No acertado! El número que buscas es <b>menor</b> que ".$_GET['numero_prueba']."<br/><br/>";
			else
				$msg_intento = "No acertado! El número que buscas es <b>mayor</b> que ".$_GET['numero_prueba']."<br/><br/>";
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1"> 
		<title>Adivina el número!</title>
		<script type="text/javascript">
			function mostrarNumero() {
				document.getElementById("pista").style.visibility = "visible";
			}
		</script>
	</head>
	<body>
		<h1>Adivina el número!</h1>
<?php if($adivino) { ?>
		<b>Felicitaciones! Has acertado al número <?php echo $buscado ?>!</b>
<?php } else if($quedan_intentos) { ?>
		<form method="GET" action="adivinar_numero.php">
			<?php 
				echo getInputIntentos($num_intentos);
				echo $msg_intento;
			?>

			Te quedan exáctamente <b><?php echo $num_intentos; ?></b> intentos.<br/><br/>
			Probar con el siguiente número:
			&nbsp;&nbsp;
			<input type="text" name="numero_prueba" style="width: 30pt;" />
			&nbsp;&nbsp;
			<input type="submit" value="Probar"/>
		</form>

		<br/><hr><br/>

		Si quieres saber el número, click acá:
		&nbsp;&nbsp;
		<input type="button" onclick="mostrarNumero()" value="Ver numero"/>
		<div id="pista" style=" visibility: hidden;">
			<b><?php echo $buscado; ?></b>
		</div>
<?php } else { ?>
		<b>No No Noo! No has acertado en 3 intentos. El número que buscabas era el <?php echo $buscado ?>.</b>
		<br/><br/>
		<a href="adivinar_numero.php">Pulsa acá para reiniciar el juego</a>
<?php } ?>
	</body>
</html>