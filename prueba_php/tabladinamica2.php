<?php
	function imprimir_tablas($max_tablas) {
		$tabla = 1;

		while($tabla <= $max_tablas) {
			echo "<tr>
					<td colspan='5'>
						<b>Tabla del {$tabla}</b>
					</td>
				</tr>";

			$mult = 1;

			while($mult <= $max_tablas) {
				$res = $tabla*$mult;
				echo "<tr>
					<td>{$tabla}</td>
					<td>x</td>
					<td>{$mult}</td>
					<td>=</td>
					<td>${res}</td>
				</tr>";

				$mult++;
			}
			
			$tabla++;
		}
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Tablas de multiplicar</title>
	</head>
	<body>
		<h1>Tablas de multiplicar</h1>
		<table>
		<?php imprimir_tablas(12); ?>
		</table>
	</body>
</html>