<?php
	$dbhost = 'localhost';
	$dbuser = 'root';
	$dbpass = '';
	$dbname = 'prueba';
	$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname);

	if($mysqli->connect_errno)
        die('No se puede conectar: ' . $mysqli->connect_error);

    $sql = 'SELECT * FROM boleta';
	$resultado = $mysqli->query($sql);

	if(!$resultado)
		die('No se pudo realizar la consulta: ' . $mysqli->error);

	while($row = $resultado->fetch_assoc()) {
      echo "ID Boleta :{$row['id']} <br> ".
         	"Fecha: {$row['fecha']} <br> ".
         	"Titular: {$row['titular']} <br> ".	
         	"--------------------------------<br>";
	}

	echo('Consulta exitosa.');
    $resultado->free();
	$mysqli->close();
?>