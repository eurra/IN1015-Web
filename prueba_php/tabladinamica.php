<!DOCTYPE html>
<html>
	<head>
		<title>Tablas de multiplicar</title>
	</head>
	<body>
		<h1>Tablas de multiplicar</h1>
		<table>
		<?php 
			$tabla = 1;

			while($tabla <= 12) {
				echo "<tr>
						<td colspan='5'>
							<b>Tabla del {$tabla}</b>
						</td>
					</tr>";

				$mult = 1;

				while($mult <= 12) {
					$res = $tabla*$mult;
					echo "<tr>
						<td>{$tabla}</td>
						<td>x</td>
						<td>{$mult}</td>
						<td>=</td>
						<td>${res}</td>
					</tr>";

					$mult++;
				}
				
				$tabla++;
			}
		?>
		</table>
	</body>
</html>